const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = express();
const mongoose = require('mongoose');
// Import and Set Nuxt.js options


const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'
mongoose.connect(`mongodb+srv://muba:dangerkhan@cluster0-fhqof.mongodb.net/mySaleDB?retryWrites=true&w=majority`, {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    })
    .then(() => {
        console.log("Connected to the Database");
    })
    .catch((error) => {
        console.log(error)
        console.log("Could not connected to the Database");
    });
async function start() {
    // Init Nuxt.js
    const nuxt = new Nuxt(config)

    const { host, port } = nuxt.options.server

    await nuxt.ready()
        // Build only in dev mode
    if (config.dev) {
        const builder = new Builder(nuxt)
        await builder.build()
    }

    // Give nuxt middleware to express
    app.use(nuxt.render)

    // Listen the server
    app.listen(port, host)
    consola.ready({
        message: `Server listening on http://${host}:${port}`,
        badge: true
    })
}
start()