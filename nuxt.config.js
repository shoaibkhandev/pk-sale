module.exports = {
    mode: 'universal',
    /*
     ** Headers of the page
     */
    head: {
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },
    // change server port 
    server: {
        port: 3001, // default: 3000
        host: 'localhost', // default: localhost,
        timing: false
    },

    serverMiddleware: ['~/api'],
    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#fff' },
    /*
     ** Global CSS
     */
    css: [
        '~/assets/css/style.css',
        // '~/assets/css/bootstrap.min.css',
        '~/assets/css/font-awesome.min.css',
        '~/assets/css/themify-icons.css',
        '~/assets/css/elegant-icons.css',
        // '~/assets/css/owl.carousel.min.css',
        '~/assets/css/nice-select.css',
        // '~/assets/css/jquery-ui.min.css',
        '~/assets/css/slicknav.min.css',
    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        { src: 'plugins/owl.js', ssr: false },
    ],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://bootstrap-vue.js.org
        'bootstrap-vue/nuxt',
    ],
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        watch: ['api'],
        extend(config, ctx) {}
    }
}